import React, { Component } from 'react';
import { Button, Modal, Form, Input, Grid, Label, Segment, Table, Checkbox } from 'semantic-ui-react';

export default class extends Component {
  render() {
    const { roles, editData, addData } = this.props;

    return (
      <Modal trigger={<Button>Manage Roles</Button>} centered={false} closeIcon>
        <Modal.Header>Roles</Modal.Header>
        <Modal.Content>
          <Form>
            {roles.sort().map((role, index) => <RoleModal key={index} data={role} editData={editData}/>)}
            <RoleModal data={{}} editData={editData} addData={addData} />
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

class RoleModal extends Component {
  constructor(props) {
    super(props);
    const { name = '', access = defaultAccessData, id = '' } = props.data || {};
    this.state = {
      name,
      access,
      show: false,
      id,
    }
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  toggle = () => {
    this.setState({
      show: !this.state.show,
    });
  }

  handleSubmit = () => {
    const { name, access, id } = this.state;
    const data = Object.assign({}, { name, access, id });

    if (id) {
      this.props.editData(data, 'role');
    } else {
      this.props.addData(data, 'role');
    }
    this.setState({
      show: false,
      name: '',
      access: defaultAccessData
    });
  }

  update = (permission) => {
    const { access } = this.state;
    const newAccess = access.map((accessData) => Object.assign({}, accessData));
    const targetPermission = newAccess.find(accessData => accessData.id === permission.id);
    targetPermission.view = permission.view;
    targetPermission.edit = permission.edit;
    this.setState({
      access: newAccess
    });
  }

  render() {
    const { name, access, show } = this.state;
    const color = name ? 'grey' : 'teal';
    const roleName = (this.props.data || {}).name;
    return (
      <div style={{marginBottom: '1em'}}>
        {!show ?
          <Button color={color} onClick={this.toggle}>{roleName || 'Add New Role'}</Button> :
          <Segment>
            <Form.Field>
              <Grid stackable>
                <Grid.Column width={2}>
                  <Label>Role Name: </Label>
                </Grid.Column>
                <Grid.Column width={6}>
                  <Input placeholder='Name' name="name" value={name} onChange={this.onChange}/>
                </Grid.Column>
              </Grid>
            </Form.Field>
            <Table celled striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Permissions</Table.HeaderCell>
                  <Table.HeaderCell>View</Table.HeaderCell>
                  <Table.HeaderCell>Edit</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {access.map(accessData => <Permission key={accessData.title} permission={accessData} update={this.update} />)}
              </Table.Body>
            </Table>
            <Button.Group>
              <Button positive onClick={this.handleSubmit}>Save</Button>
              <Button.Or />
              <Button negative onClick={this.toggle}>Cancel</Button>
            </Button.Group>
          </Segment>
        }
      </div>
    )
  }
}

class Permission extends Component {
  constructor(props) {
    super(props);
    const { view = false, edit = false, title = '', id } = props.permission || {};
    this.state = {
      view,
      edit,
      title,
      id,
    }
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    }, () => {
      const { id, title, edit, view } = this.state;
      const permission = Object.assign({}, {id, edit, view, title});
      this.props.update(permission);
    });
  }

  render() {
    const { title, view, edit } = this.state;
    return (
      <Table.Row key={title}>
        <Table.Cell>
          <strong>{title}</strong>
        </Table.Cell>
        <Table.Cell>
          <Checkbox toggle name="view" checked={view} onChange={this.onChecked}/>
        </Table.Cell>
        <Table.Cell>
          <Checkbox toggle name="edit" checked={edit} onChange={this.onChecked}/>
        </Table.Cell>
      </Table.Row>
    )
  }
}

const defaultAccessData = [{
  title: 'Admin',
  view: false,
  edit: false,
  id: 'admin',
}, {
  title: 'User',
  view: false,
  edit: false,
  id: 'user',
}];