import React, { Component } from 'react';
import { Segment, Button } from 'semantic-ui-react';
import { MDBDataTable } from 'mdbreact';
import EditModal from './editModal';
import moment from 'moment';

const columns = [{
  label: 'Event Status',
  field: 'status',
  sort: 'disabled',
}, {
  label: 'Name',
  field: 'name',
  sort: 'asc',
}, {
  label: 'Capacity',
  field: 'capacity',
}, {
  label: 'Address',
  field: 'address',
}, {
  label: 'Host',
  field: 'host',
  sort: 'disabled',
}, {
  label: 'Performance At',
  field: 'performanceAt',
  sort: 'disabled',

}, {
  label: 'Actions',
  field: 'actions',
  sort: 'disabled',
}];

export default class extends Component {
  render() {
    const { events, editData, routesAccess, users } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'event');
    const { view, edit } = currentRouteAccess || {};
    if (!view && !edit) {
      return null;
    }

    const rows = events.map(dataRow => {
      const { isDisabled, name, capacity, host, location, performanceAt } = dataRow;
      const toggleData = Object.assign({}, dataRow, {
        isDisabled: !dataRow.isDisabled
      });

      return {
        status: isDisabled ? 'Inactive': 'Active',
        name,
        capacity,
        address: `${location.fullAddress}, ${location.city}`,
        host: host ? host.name : '',
        performanceAt: <span style={{whiteSpace: 'nowrap'}}>{moment(performanceAt).format('DD-MMM-YY hh:mm a')}</span>,
        actions: (
          <div style={{display: 'flex'}}>
            <EditModal canEdit={edit} data={dataRow} editData={editData} users={users}/>
            {edit &&
              <Button onClick={() => {editData(toggleData, 'event')}}>
                <Button.Content>{isDisabled ? 'Enable': 'Disable'}</Button.Content>
              </Button>
            }
          </div>
        )
      }
    });
    const tableData = {
      columns,
      rows
    };

    return (
      <Segment>
        <MDBDataTable responsive striped bordered hover data={tableData} />
      </Segment>
    )
  }
}