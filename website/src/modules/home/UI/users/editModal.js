import React, { Component } from 'react';
import { Button, Modal, Form, Input, Label } from 'semantic-ui-react';

export default class extends Component {
  constructor(props) {
    super(props);
    const { email = '', firstName = '', lastName = '' } = props.data;
    this.state = {
      email,
      firstName,
      lastName,
      mobileNo: props.data.mobileNo || '',
      password: props.data.password || '',
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  handleSubmit = () => {
    const { facebookId = '', googleId = '' } = this.props;
    const { email, password, firstName, lastName, mobileNo } = this.state;
    const { id } = this.props.data;
    const data = Object.assign({}, { email, password, firstName, lastName, facebookId, googleId, mobileNo }, { id });
    this.props.editData(data, 'user');
  }

  render() {
    const { data, canEdit } = this.props;
    const { facebookId = '', googleId = '' } = data;
    const { email, firstName, lastName, mobileNo, password } = this.state;

    return (
      <Modal trigger={<Button icon='edit' color='blue'/>} centered={false} closeIcon>
        <Modal.Header>Edit User</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <label>First Name</label>
              <Input placeholder='First Name' name="firstName" value={firstName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Last Name</label>
              <Input placeholder='Last Name' name="lastName" value={lastName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Email</label>
              <Input placeholder='Email' name="email" value={email} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Mobile Number</label>
              <Input placeholder='Mobile Number' name="mobileNo" value={mobileNo} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <Input placeholder='Password' name="password" value={password} onChange={this.onChange}/>
            </Form.Field>
            {facebookId &&
              <Form.Field>
                <label>Facebook ID</label>
                <Label>{facebookId}</Label>
              </Form.Field>
            }
            {googleId &&
              <Form.Field>
                <label>Google ID</label>
                <Label>{googleId}</Label>
              </Form.Field>
            }
            {canEdit && <Button type='submit'>Submit</Button>}
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}