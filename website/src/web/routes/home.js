import HomeComponent from '../../modules/home/UI/index';
import HomeContainer from '../../containers/home';

import AdminComponent from '../../modules/home/UI/admins/index';
import UserComponent from '../../modules/home/UI/users/index';
import EventComponent from '../../modules/home/UI/events/index';
import VendorComponent from '../../modules/home/UI/vendors/index';

import { TEMPLATE_TYPES } from '../template/index';

const homeRoute = {
  Component: HomeComponent,
  Container: HomeContainer,
  path: '/',
  title: '',
  exact: true,
  templateType: TEMPLATE_TYPES.PURE,
};

const userRoute = {
  Component: UserComponent,
  Container: HomeContainer,
  path: '/users',
  title: 'Users',
  templateType: TEMPLATE_TYPES.PURE,
};

const eventRoute = {
  Component: EventComponent,
  Container: HomeContainer,
  path: '/events',
  title: 'Events',
  templateType: TEMPLATE_TYPES.PURE,
};

const adminRoute = {
  Component: AdminComponent,
  Container: HomeContainer,
  path: '/admins',
  title: 'Admins',
  templateType: TEMPLATE_TYPES.PURE,
};

const vendorRoute = {
  Component: VendorComponent,
  Container: HomeContainer,
  path: '/vendors',
  title: 'Vendors',
  templateType: TEMPLATE_TYPES.PURE,
};

export default [
  homeRoute,
  adminRoute,
  userRoute,
  vendorRoute,
  eventRoute,
];