export default {
  loading: false,
  error: null,
  user: {},
  isAuthenticated: false,
  token: null,
  passwordUpdateError: null,
  updatePasswordSuccess: false,
};