import Store from '../store/events';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function eventsReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_EVENTS_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
        data: [],
      });
    case TYPES.FETCH_EVENTS_SUCCESS:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: null,
      });
    case TYPES.FETCH_EVENTS_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        data: [],
      });
    default:
      return state;
  }
}