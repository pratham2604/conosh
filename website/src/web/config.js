import { TEMPLATE_TYPES } from './template/index';

const firebaseConfig = {
  apiKey: "AIzaSyBvVGLXElg0edE5KDNYxK3FS6bBBIxGSKs",
  authDomain: "conosh-439fc.firebaseapp.com",
  databaseURL: "https://conosh-439fc.firebaseio.com",
  projectId: "conosh-439fc",
  storageBucket: "conosh-439fc.appspot.com",
  messagingSenderId: "1058486260256",
  appId: "1:1058486260256:web:57a3c5410a023c6ecba245",
  measurementId: "G-JJ87ZW282E"
};

export default {
  DEFAULT_TEMPLATE: TEMPLATE_TYPES.NAVBAR,
  FIREBASE_CONFIG: firebaseConfig,
};