import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';

export const fetchEvents = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_EVENTS_REQUEST});
    Firestore.collection("events").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_EVENTS_SUCCESS, data});
    });
  }
}

export const addEvents = (data) => {
  return (dispatch) => {
    data.id = uuidv4();
    Firestore.collection("events").doc(data.id).set(data)
    .then((docRef) => {
      toast.success('Event added successfully');
      console.log("Document written with ID: ", docRef.id);
    }).catch((error) => {
      toast.error(error);
      console.error("Error adding document: ", error);
    });
  }
}

export const updateEvents = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("events").doc(id).update(data)
    .then((snapshot) => {
      toast.success('Event updated successfully');
      console.log("Doc updated")
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}