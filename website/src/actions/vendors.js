import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';

export const addVendor = (data) => {
  return (dispatch) => {
    data.id = uuidv4();
    Firestore.collection("vendors").doc(data.id).set(data)
    .then((docRef) => {
      toast.success('Vendor added successfully');
      Firestore.collection("vendor_categories").doc(data.category).get().then((doc) => {
        const data = doc.data();
        const updatedData = Object.assign({}, data, {
          vendorsCount: data.vendorsCount + 1,
        })
        Firestore.collection("vendor_categories").doc(data.id).update(updatedData).then(snapshot => {
          console.log(snapshot);
        })
      }).catch(error => {
        toast.error(error);
      });
    }).catch((error) => {
      toast.error(error);
      console.error("Error adding document: ", error);
    });
  }
}

export const fetchVendors = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_VENDORS_REQUEST});
    Firestore.collection("vendors").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_VENDORS_SUCCESS, data});
    });
  }
}

export const fetchVendorsCategories = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_VENDORS_CATEGORIES_REQUEST});
    Firestore.collection("vendor_categories").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_VENDORS_CATEGORIES_SUCCESS, data});
    });
  }
}

export const updateVendorsCategories = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("vendor_categories").doc(id).update(data)
    .then((snapshot) => {
      console.log("Doc updated")
      toast.success('Vendor Category updated successfully');
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}

export const addVendorCategories =  (data) => {
  return (dispatch) => {
    Firestore.collection("vendor_categories").doc(data.id).set(data)
    .then((docRef) => {
      console.log("Document written with ID: ", docRef.id);
      toast.success('Vendor Category added successfully');
    }).catch((error) => {
      toast.error(error);
      console.error("Error adding document: ", error);
    });
  }
}

export const updateVendor = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("vendors").doc(id).update(data)
    .then((snapshot) => {
      toast.success('Vendor updated successfully');
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}