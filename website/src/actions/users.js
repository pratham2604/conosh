import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { toast } from 'react-toastify';

export const fetchUsers = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_USERS_REQUEST});
    Firestore.collection("users").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_USERS_SUCCESS, data});
    });
  }
}

export const updateUser = (data) => {
  const { id } = data;
  return (dispatch) => {
    dispatch({type: TYPES.UPDATE_USERS_REQUEST});
    Firestore.collection("users").doc(id).update(data)
    .then((snapshot) => {
      toast.success('User updated successfully');
      dispatch({type: TYPES.UPDATE_USERS_SUCCESS});
    }).catch((error) => {
      console.error("Error loading documents: ", error);
      const message = 'Error loading users data';
      toast.error(message);
      dispatch({type: TYPES.UPDATE_USERS_FAILURE, message});
    });
  }
}