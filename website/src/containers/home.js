import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchAuth, signOut } from '../actions/auth';
import { fetchAdmin, addAdmin, updateAdmin, fetchRoles, updateRoles, addRoles } from '../actions/admins';
import { fetchEvents, addEvents, updateEvents } from '../actions/events';
import { fetchVendors, addVendor, updateVendor, fetchVendorsCategories, updateVendorsCategories, addVendorCategories } from '../actions/vendors';
import { fetchUsers, updateUser } from '../actions/users';
import SideMenu from '../components/sideMenu';

const editMethodMap = {
  vendor: updateVendor,
  admin: updateAdmin,
  event: updateEvents,
  user: updateUser,
  category: updateVendorsCategories,
  role: updateRoles,
};

const addMethodMap = {
  vendor: addVendor,
  admin: addAdmin,
  event: addEvents,
  category: addVendorCategories,
  role: addRoles,
}

class Home extends Component {
  state = {
    loadComponent: false,
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchAuth(this.onSuccess, this.onFailure));
    dispatch(fetchAdmin());
    dispatch(fetchUsers());
    dispatch(fetchVendors());
    dispatch(fetchEvents());
    dispatch(fetchVendorsCategories());
    dispatch(fetchRoles());
  }

  onSuccess = (data) => {
    this.setState({
      loadComponent: true,
    });
  }

  onFailure = () => {
    const { history } = this.props;
    history.push('/login');
  }

  signOut = () => {
    const { dispatch } = this.props;
    dispatch(signOut(this.onSingOutSuccess, this.onSignOutFailure));
  }

  onSingOutSuccess = () => {
    const { history } = this.props;
    history.push('/login');
  }

  onSignOutFailure = (error) => {
    console.log(error);
  }

  addData = (data, type) => {
    const { dispatch } = this.props;
    dispatch(addMethodMap[type](data));
  }
  
  editData = (data, type) => {
    const { dispatch } = this.props;
    dispatch(editMethodMap[type](data));
  }

  render() {
    const { loadComponent } = this.state;
    const { Layout, location, ...rest } = this.props;
    const { auth, admins, roles } = this.props;
    if (!loadComponent) {
      return null;
    };

    const currentAdmin = admins.find(admin => admin.email === auth.email);
    const currentUser = Object.assign({}, auth, currentAdmin);
    const currentUserRole = (roles || []).find(role => role.id === currentUser.role);
    const routesAccess = (currentUserRole || {}).access || [];

    return (
      <SideMenu auth={auth} admins={admins} roles={roles} location={location} signOut={this.signOut}>
        <Layout
          loadData={this.loadData}
          editData={this.editData}
          addData={this.addData}
          routesAccess={routesAccess}
          {...rest}
        />
      </SideMenu>
    )
  }
}

const mapStateToProps = state => ({
  users: state.users.data,
  events: state.events.data,
  admins: state.admins.data,
  vendors: state.vendors.data,
  auth: state.auth.user,
  categories: state.vendors.categories,
  roles: state.admins.roles,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));