import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { login } from '../actions/auth';

class Auth extends Component {
  state = {
    loginError: null,
  }

  login = (data) => {
    const { dispatch } = this.props;
    this.setState({
      loginError: null,
    });
    dispatch(login(data, this.onLoginSuccess, this.onLoginFailure))
  }

  onLoginSuccess = (data) => {
    const { history } = this.props;
    history.push('/');
  }

  onLoginFailure = (error) => {
    this.setState({
      loginError: error,
    });
  }

  render() {
    const { Layout } = this.props;
    const { loginError } = this.state;

    return (
      <Layout
        login={this.login}
        loginError={loginError}
      />
    )
  }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Auth));